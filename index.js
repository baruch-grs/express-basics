const express = require('express');
const morgan = require('morgan');
const app = express();

//Settings
app.set('appName', 'Fazt express tutorial');
app.set('port', 3000);
app.set('view engine', 'ejs');

// Middlewares
app.use(express.json());
app.use(morgan('dev'));



// Routes

// app.all('/user', (req, res, next) => {
//     console.log('Por aquí pasó');
//     next();
// });

app.get('/', (req, res) => {
    const data = [{name : 'Jhon'}, {name: 'Joe'}, {name: 'Cameron'}, {name: 'Ryan'}];
    res.render('index.ejs', {people: data});
})
app.get('/user', (req, res) => {
    res.json({
        username:'Cameron',
        lastname: 'Díaz,'
    });
});

app.post('/user/:id', (req, res) => {
    console.log(req.body);
    console.log(req.params);
    res.send('POST RECEIVED');
});
app.put('/user/:userId', (req, res) => {
    console.log(req.body);
    res.send(`User ${req.params.userId} updated`);
});
app.delete('/user/:userId', (req, res) => {
    res.send(`User ${req.params.userId} deleted`);
});

app.use(express.static('public'));

app.listen(app.get('port'), () => {
    console.log(app.get('appName'));
    console.log(`Server on port ${app.get('port')}`);
});